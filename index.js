const { Client } = require('pg');

class MyRdbms {
#email = 'nee@mail.com';
    #password = '12345';

    constructor(userObj) {
      this.client = new Client({
            user: 'postgres',
            host: 'localhost',
            password: 'nelydwia18',
            database: 'spapp',
            port: 5432,
        });
        this.authenticated = this.#authenticate(userObj);
    }

      #authenticate(userObj) {
        return (this.#email === userObj.email) && (this.#password === userObj.password); 
    }

    
    createDatabase(databaseName) {
        if (this.authenticated) {
            const queryCreateDatabase = `CREATE DATABASE ${databaseName};`;

            this.client.connect();

            return this.client.query(queryCreateDatabase, (err, res) => {
                if (err)  {
                    this.client.end();
                    this.client.database = databaseName;
                    return console.log(err);
                }
                this.client.end();
                this.client.database = databaseName;
                return console.log('Sukses membuat database');
              })
        }
        return console.log('Anda tidak memiliki hak!')
    }

    createTable() {
        if (this.authenticated) {
            const queryCreateTable = `
            CREATE TABLE staffs (
              staff_id int NOT NULL,
              name char ( 50 ) NOT NULL,
              city char ( 50 ) NOT NULL,
              PRIMARY KEY (staff_id)
            );

            CREATE TABLE accounts (
              account_id int NOT NULL,
              balance int NOT NULL,
              staff_id int NOT NULL,
              PRIMARY KEY (account_id),
              FOREIGN KEY (staff_id) REFERENCES staffs(staff_id)
            );

            CREATE TABLE costumers (
              costumer_id int NOT NULL,
              address text NOT NULL,
              account_id int NOT NULL,
              PRIMARY KEY (costumer_id),
              FOREIGN KEY (account_id) REFERENCES accounts(account_id)
            );
            `
            this.client.connect();
            return this.client.query(queryCreateTable, (err, res) => {
                if (err)  {
                    this.client.end();
                    return console.log('Gagal membuat table');
                }
                this.client.end();
                return console.log('Sukses membuat table');
            })
        }
        return console.log('Anda tidak memiliki hak!')
    }

    insertTable(){
      if (this.authenticated) {
          const queryInsertEmployees = `
          INSERT INTO costumers(costumer_id, address, account_id)
          values(1, 'Ponorogo', 1);
          `
          
          this.client.connect();
          return this.client.query(queryInsertEmployees, (err, res) =>{
              if (err) {
                  this.client.end();
                  return console.log('Gagal insert table');
              }
              this.client.end();
              return console.log('Sukses insert table');
          })
      }
      return console.log('Maaf lurr anda tidak punya akses!');

  }
    selectTable(){
      if (this.authenticated){
          const querySelectTable = `SELECT * FROM staffs;`;
          this.client.connect();
          return this.client.query(querySelectTable, (err, res)=>{
              if (err) {
                  this.client.end();
                  return console.log('Gagal insert table');
              }
              this.client.end();
              return console.log('Sukses Select Table');  
          })
      }
      return console.log('Maaf lurrr anda tidak punya akses!!');

  }

    updateTable(){
      if (this.authenticated){
          const queryUpdateTable = `UPDATE staffs
          SET name = 'Alfred Schmidt', city= 'Konohagakure'
          WHERE staff_id = 1;`;
          this.client.connect();
          return this.client.query(queryUpdateTable, (err, res)=>{
              if (err) {
                  this.client.end();
                  return console.log('Gagal Update table');
              }
              this.client.end();
              return console.log('Sukses Update Table');  
          })
      }
      return console.log('Maaf lurrr anda tidak punya akses!!');

  }

    deleteTable(){
      if (this.authenticated){
          const queryDelete = `DELETE FROM staffs WHERE id= 1;`;
          this.client.connect();
          return this.client.query(queryDelete, (err, res)=>{
              if (err) {
                  this.client.end();
                  return console.log('Gagal Delete table');
              }
              this.client.end();
              return console.log('Sukses Delete Table');  
          })
      }
      return console.log('Maaf lurrr anda tidak punya akses!!');
  }

  inerJoinTable(){
    if (this.authenticated){
        const queryInerJoin = `SELECT * FROM costumers JOIN accounts ON costumers.account_id = accounts.account_id;`;
        this.client.connect();
        return this.client.query(queryInerJoin, (err, res)=>{
            if (err) {
                this.client.end();
                return console.log('Gagal Iner Join table');
            }
            this.client.end();
            return console.log(`${queryInerJoin}`);  
        })
    }
    return console.log('Maaf lurrr anda tidak punya akses!!');
}
}



const adminPertama = new MyRdbms({ email: 'nee@mail.com', password: '12345' });
// adminPertama.createDatabase('spapp');
// adminPertama.createTable();
// adminPertama.insertTable();
// adminPertama.selectTable();
// adminPertama.updateTable();
// adminPertama.deleteTable();
adminPertama.inerJoinTable();
